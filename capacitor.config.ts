import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ivan.osteo',
  appName: 'osteo',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
