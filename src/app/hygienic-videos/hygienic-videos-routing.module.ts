import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HygienicVideosPage } from './hygienic-videos.page';

const routes: Routes = [
  {
    path: '',
    component: HygienicVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HygienicVideosPageRoutingModule {}
