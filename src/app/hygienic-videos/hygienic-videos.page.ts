import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-hygienic-videos',
  templateUrl: './hygienic-videos.page.html',
  styleUrls: ['./hygienic-videos.page.scss'],
})
export class HygienicVideosPage implements OnInit {
  hygieneVideos: any = [];
  isVideos: boolean = true;

  constructor(public global: GlobalService, private apiService: ApiService, private navController: NavController) { }

  ngOnInit() {
    this.getChildVideos();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  getChildVideos() {
    this.global.presentLoadingDefault();
    this.apiService.hygieneVideos().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout()
        }else{
          this.hygieneVideos = data.data;
          this.isVideos = data.data.length > 0 ? true : false;
        }
      }
    })
  }

  goToDetails(item:any) {
    let navigationExtras: NavigationExtras = {
      state: {
        detail: item,
        categoryType: 'hygienic'
      }
    };
    this.navController.navigateForward(["/video-and-details"], navigationExtras);
  }
}
