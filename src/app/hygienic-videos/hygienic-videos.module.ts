import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HygienicVideosPageRoutingModule } from './hygienic-videos-routing.module';

import { HygienicVideosPage } from './hygienic-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HygienicVideosPageRoutingModule
  ],
  declarations: [HygienicVideosPage]
})
export class HygienicVideosPageModule {}
