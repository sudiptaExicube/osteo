import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogVideoPage } from './blog-video.page';

const routes: Routes = [
  {
    path: '',
    component: BlogVideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogVideoPageRoutingModule {}
