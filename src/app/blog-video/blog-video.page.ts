// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../services/global.service';
import {YouTubePlayerModule} from '@angular/youtube-player';
import { FeedbackModalPage } from '../feedback-modal/feedback-modal.page';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-blog-video',
  templateUrl: './blog-video.page.html',
  styleUrls: ['./blog-video.page.scss'],
})
export class BlogVideoPage implements OnInit {

  @ViewChild('player') player: any;
  detail: any = {};
  showBackdrop = true;
  apiLoaded = false;
  videoId: any = '';
  // categoryType: any = '';

  constructor(
    public navController: NavController, 
    private router: Router, 
    public global: GlobalService,
    public sanitizer:DomSanitizer
  ) {
    
    // let paramdata = this.router.getCurrentNavigation().extras.state.detail;
    this.detail = this.router.getCurrentNavigation().extras.state.detail;
    // this.detail = this.sanitizer.bypassSecurityTrustHtml(paramdata)


    // this.categoryType = this.router.getCurrentNavigation().extras.state.categoryType;
    // console.log(this.detail, this.categoryType);
    this.videoId = this.detail.video.split('/').pop();
    console.log("Video id is : ", this.videoId);
  }

  ngOnInit() {
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
  }

  // Loop
  onStateChange(event) {
    // console.log(event);
    if (event.data === 0) {
      // this.presentFeedbackModal();
    }
  }



}
