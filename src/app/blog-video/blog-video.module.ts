import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlogVideoPageRoutingModule } from './blog-video-routing.module';

import { BlogVideoPage } from './blog-video.page';
import {YouTubePlayerModule} from '@angular/youtube-player';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlogVideoPageRoutingModule,
    YouTubePlayerModule
  ],
  declarations: [BlogVideoPage]
})
export class BlogVideoPageModule {}
