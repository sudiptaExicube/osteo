import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoAndDetailsPageRoutingModule } from './video-and-details-routing.module';

import { VideoAndDetailsPage } from './video-and-details.page';
import {YouTubePlayerModule} from '@angular/youtube-player';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideoAndDetailsPageRoutingModule,
    YouTubePlayerModule
  ],
  declarations: [VideoAndDetailsPage]
})
export class VideoAndDetailsPageModule {}
