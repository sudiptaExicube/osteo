import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../services/global.service';
import {YouTubePlayerModule} from '@angular/youtube-player';
import { FeedbackModalPage } from '../feedback-modal/feedback-modal.page';

@Component({
  selector: 'app-video-and-details',
  templateUrl: './video-and-details.page.html',
  styleUrls: ['./video-and-details.page.scss'],
})
export class VideoAndDetailsPage implements OnInit {
  @ViewChild('player') player: any;
  detail: any = {};
  showBackdrop = true;
  apiLoaded = false;
  videoId: any = '';
  categoryType: any = '';

  constructor(public navController: NavController, private router: Router, public global: GlobalService) {
    this.detail = this.router.getCurrentNavigation().extras.state.detail;
    this.categoryType = this.router.getCurrentNavigation().extras.state.categoryType;
    console.log(this.detail, this.categoryType);
    this.videoId = this.detail.video.split('/').pop();
    console.log("Video id is : ", this.videoId);
  }

  ngOnInit() {
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  // onReady() {
    // this.player.mute();         
    // this.player.playVideo();    
  // }

  // Loop
  onStateChange(event) {
    // console.log(event);
    if (event.data === 0) {
      this.presentFeedbackModal();
      // if(this.detail.feedback)
      //   this.presentFeedbackModal();
      // else
      //   this.noFeedbackAlert();
    }
  }

  async presentFeedbackModal() {
    const modal = await this.global.modalController.create({
      component: FeedbackModalPage,
      cssClass: 'user-feedback-modal-class',
      componentProps: {
        'categoryType': this.categoryType,
        'vid': this.detail.id
      }
    });

    modal.onDidDismiss().then(data => {
      console.log('dismissed', data);
      // this.global.isfeedbackModal = false;
      // this.navController.navigateRoot('/user-type-category');
    });

    modal.addEventListener('ionModalWillDismiss', (event: any) => {
      console.log('works'); //shows the message when closing the modal using modal.dismiss() 
      // event.preventDefault(); //does not block the closing of the modal when using dismiss, i.e. it closes it when it should stop the event.
      this.navController.navigateRoot('/user-type-category');
    });

    return await modal.present();
    
  }

  async noFeedbackAlert() {
    const alert = await this.global.alertCtrl.create({
      cssClass: 'play-again-custom-class',
      backdropDismiss: false,
      // header: 'Alert',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: [{
        text: 'Vous jouez à nouveau',
        handler: () => {
          this.navController.navigateRoot(["/user-type-category"]);
        }
      }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
