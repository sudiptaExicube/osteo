import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoAndDetailsPage } from './video-and-details.page';

const routes: Routes = [
  {
    path: '',
    component: VideoAndDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideoAndDetailsPageRoutingModule {}
