import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-feedback-modal-health',
  templateUrl: './feedback-modal-health.page.html',
  styleUrls: ['./feedback-modal-health.page.scss'],
})
export class FeedbackModalHealthPage implements OnInit {
  selectedEmoji: any = '';
  @Input() type: any;

  constructor(public global: GlobalService, private apiService: ApiService, private nativeStorage: NativeStorage, public navController: NavController) { }

  ngOnInit() {
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  selectFeedback(selected:any) {
    this.selectedEmoji = selected;
  }

  sendFeedback() {
    if(!this.selectedEmoji) {
      this.global.presentToast('Veuillez sélectionner un emoji');
      return false;
    }
    this.global.presentLoadingDefault();
    this.apiService.sendHealthFeedback(this.selectedEmoji, this.type).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      this.global.presentToast(data.message);
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.global.modalController.dismiss({
            'dismissed': true
          });
          if(this.type == 'logout') {
            this.global.isLoggedIn = false;
            this.global.userdetails = {};
            this.global.user_access_token = '';
            this.nativeStorage.setItem('userdetails', null);
            this.navController.navigateRoot('/login');
            setTimeout(() => {
              this.global.presentToast('Déconnecté avec succès');
            }, 3000);
          }
        }

      }
    })
  }
}
