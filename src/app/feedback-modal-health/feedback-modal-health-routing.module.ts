import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackModalHealthPage } from './feedback-modal-health.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackModalHealthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackModalHealthPageRoutingModule {}
