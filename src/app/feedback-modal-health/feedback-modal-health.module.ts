import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackModalHealthPageRoutingModule } from './feedback-modal-health-routing.module';

import { FeedbackModalHealthPage } from './feedback-modal-health.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackModalHealthPageRoutingModule
  ],
  declarations: [FeedbackModalHealthPage]
})
export class FeedbackModalHealthPageModule {}
