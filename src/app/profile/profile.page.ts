import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  profileForm: FormGroup;
  public countryIso:any;

  constructor(private formBuilder: FormBuilder, public navController: NavController, public global: GlobalService, private apiService: ApiService, private camera: Camera, private crop: Crop, private zone: NgZone, private transfer: FileTransfer, private nativeStorage: NativeStorage) {
    console.log( this.global.userdetails);

		this.profileForm = this.formBuilder.group({
      lastname: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z\s]*$/)
      ])],
      firstname: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z\s]*$/)
      ])],
      telephone: ['', [Validators.compose([Validators.pattern(/^([0|\+[0-9]{1,5})?([0-9][0-9]{8})$/), Validators.required])]],
      email: [{value: '',disabled: true}, Validators.required]
    });

    if(this.global.allCountryList.lenght == 0){
      this.fetchCountrylist();
    }

  }
  

  public goToPaymentHistory() {
    this.navController.pop().then(()=>{
    this.navController.navigateForward(["/payment-history"]);
    })
  }
  public goToMyHistory() {
    this.navController.navigateForward(["/my-history"]);
  }

    /* == Fetch country list == */
    fetchCountrylist(){
      this.global.presentLoadingDefault();
      this.apiService.fetchCountryList()
        .then((success:any)=>{
          this.global.presentLoadingClose();
            console.log("country success : ", success);
            if(success.status){
              if(success.status ==400){
                if(success.error){
                  if(success.error.message){
                    this.global.presentToast(success.error.message);
                  }
                }
              }else if(success.status ==401){
                // this.callTokenExpFunc();
                this.forceLogout();
              }else{
                this.global.allCountryList = success.data;
              }
            }
        })
        .catch((error:any)=>{
          this.global.presentLoadingClose();
          console.error(error);
        })
    }

  ngOnInit() {
    this.profileForm.patchValue({"email": this.global.userdetails.email, "lastname": this.global.userdetails.lname, "firstname": this.global.userdetails.fname, "telephone": this.global.userdetails.phone});

    if(this.global.userdetails){
      if(this.global.userdetails.country){
          this.countryIso = this.global.userdetails.country;
      }
    }

  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  async changeImage() {
    const actionSheet = await this.global.actionSheetCtrl.create({
      header: 'Choisissez une photo parmi',
      buttons: [
      {
        text: 'Galerie',
        icon: 'image',
        handler: () => {
          console.log('Gallery clicked');
          this.addimage(0);
        }
      }, {
        text: 'Caméra',
        icon: 'camera',
        handler: () => {
          console.log('Camera clicked');
          this.addimage(1);
        }
      }, {
        text: 'Annuler',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  
  addimage(source:any) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: 1,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType:source,
    }
    
    this.camera.getPicture(options).then((imageData) => {
      this.crop.crop(imageData, {quality: 100})
      .then(
      newImage => {
        console.log(newImage);
        this.saveimage(newImage);
      },
      error => {
        this.global.presentToast("Échec du téléchargement de l'image");
      });
      
    }, (err) => {
      this.global.presentToast("Échec du téléchargement de l'image");
    });
  }

  saveimage(imageurl:any){
    this.global.presentLoadingDefault();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {};
    options.fileName = this.global.userdetails.fname+this.global.userdetails.lname+'.jpeg';
    options.httpMethod = "POST";
    options.fileKey = "image_file";
    options.mimeType= "image/jpeg";
    options.chunkedMode = false;
    options.headers = { Authorization: this.global.userdetails.user_token };
    // options.params = {
    //   child_id: this.childDetails.child_personal_info.child_id
    // };

    console.log(options);

    fileTransfer.upload(imageurl, encodeURI(this.apiService.uri+'/profile-image'), options)
    .then((data:any) => {
      console.log(data);
      let dataobj:any = JSON.parse(data.response);
      console.log(dataobj);
      if(dataobj.status) {
        this.zone.run(() => {
          this.global.userdetails.profile_img = dataobj.data.image_name;
          setTimeout(() => {
            this.nativeStorage.setItem('userdetails', this.global.userdetails)
            .then(
              (data) => console.log('Stored item!', data),
              error => console.error('Error storing item', error)
            );
          }, 1000);
        });
      }
      this.global.presentToast(dataobj.message);
      this.global.presentLoadingClose();
    }, (error:any) => {
      console.log(error);
      this.global.presentLoadingClose();
      this.global.errorresponse(error);
    })
  }

  save() {
    console.log(this.profileForm.value);
    this.global.presentLoadingDefault();
    this.apiService.profileUpdate(this.profileForm.value).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      this.global.presentToast(data.message);
      if(data.status) {
        this.zone.run(() => {
          this.global.userdetails.lname = data.data.lname;
          this.global.userdetails.fname = data.data.fname;
          this.global.userdetails.phone = data.data.phone;
          setTimeout(() => {
            this.nativeStorage.setItem('userdetails', this.global.userdetails)
            .then(
              (data) => console.log('Stored item!', data),
              error => console.error('Error storing item', error)
            );
          }, 1000);
        });
      }
    });
  }
}
