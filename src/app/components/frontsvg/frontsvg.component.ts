import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-frontsvg',
  templateUrl: './frontsvg.component.html',
  styleUrls: ['./frontsvg.component.scss'],
})
export class FrontsvgComponent implements OnInit {
  // fillColor = 'rgb(0, 0, 0)';
  selected: any = 0;

  constructor(public global: GlobalService, private navController: NavController) { }
  ngOnInit() {}

  tapOnBodyPart(svgPartid:any, name:any, bodyPartId:any) {
    this.selected = svgPartid;
    this.global.bodyPartSelected = name;
    this.global.bodyPartSelectedId = bodyPartId;
    this.getBodyPartsVideos();
  }

  getBodyPartsVideos() {
    this.navController.navigateForward(["/body-parts-videos"]);
  }
}
