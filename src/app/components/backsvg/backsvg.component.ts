import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-backsvg',
  templateUrl: './backsvg.component.html',
  styleUrls: ['./backsvg.component.scss'],
})
export class BacksvgComponent implements OnInit {
  selected: any = 0;

  constructor(public global: GlobalService, private navController: NavController) { }

  ngOnInit() {}

  tapOnBodyPart(svgPartid:any, name:any, bodyPartId:any) {
    this.selected = svgPartid;
    this.global.bodyPartSelected = name;
    this.global.bodyPartSelectedId = bodyPartId;
    this.getBodyPartsVideos();
  }

  getBodyPartsVideos() {
    this.navController.navigateForward(["/body-parts-videos"]);
  }
}
