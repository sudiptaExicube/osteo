import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-human-body',
  templateUrl: './human-body.page.html',
  styleUrls: ['./human-body.page.scss'],
})
export class HumanBodyPage implements OnInit {
  selectedIndex: any = 1;

  constructor(public global: GlobalService, private navController: NavController, private apiService: ApiService) { }

  ngOnInit() {
    // this.getBodyParts();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  selectItem(index:any) {
    this.selectedIndex = index;
    this.global.bodyPartSelected = '';
  }

  ionViewDidLeave() {
    // this.global.bodyPartSelected = '';
  }

  goToPhysicalExercise() {
    if(!this.global.bodyPartSelected) {
      this.global.presentToast('Sélectionnez une partie du corps pour regarder la vidéo');
      return false;
    }
    this.navController.navigateForward(["/body-parts-videos"]);
  }

  getBodyParts() {
    this.global.presentLoadingDefault();
    this.apiService.valeriabodyParts().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }

      }
    })
  }
}
