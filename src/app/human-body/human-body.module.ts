import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HumanBodyPageRoutingModule } from './human-body-routing.module';

import { HumanBodyPage } from './human-body.page';
import { BacksvgComponent } from '../components/backsvg/backsvg.component';
import { FrontsvgComponent } from '../components/frontsvg/frontsvg.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HumanBodyPageRoutingModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [FrontsvgComponent, BacksvgComponent],
  declarations: [HumanBodyPage, FrontsvgComponent, BacksvgComponent]
})
export class HumanBodyPageModule {}
