import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HumanBodyPage } from './human-body.page';

const routes: Routes = [
  {
    path: '',
    component: HumanBodyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HumanBodyPageRoutingModule {}
