import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeTestimonialPage } from './welcome-testimonial.page';

const routes: Routes = [
  {
    path: '',
    component: WelcomeTestimonialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WelcomeTestimonialPageRoutingModule {}
