import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-welcome-testimonial',
  templateUrl: './welcome-testimonial.page.html',
  styleUrls: ['./welcome-testimonial.page.scss'],
})
export class WelcomeTestimonialPage implements OnInit {
  videoList: any = [];
  testimonials: any = [];
  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1.2,
    spaceBetween: 20
  };
  platformis: any = '';

  constructor(public menu: MenuController, public navController: NavController, private router: Router, public global: GlobalService, private apiService: ApiService, public platform: Platform) {
    this.menu.swipeGesture(true);
    this.platformis = this.platform.is('android') ? 'a' : 'i';
  }

  ngOnInit() {
    this.global.rootpage = 'home';
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  ionViewDidEnter() {
    this.getFreeVideos();
    this.getTestimonial();
  }

  getFreeVideos() {
    this.global.presentLoadingDefault();
    this.apiService.getFreeVideos().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.videoList = data.data;
        }
      }
    });
  }

  getTestimonial() {
    this.apiService.testimonial().then((data:any) => {
      console.log(data);
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.testimonials = data.data;
        }
      }
    });
  }
}
