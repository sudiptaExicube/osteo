import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WelcomeTestimonialPageRoutingModule } from './welcome-testimonial-routing.module';

import { WelcomeTestimonialPage } from './welcome-testimonial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WelcomeTestimonialPageRoutingModule
  ],
  declarations: [WelcomeTestimonialPage]
})
export class WelcomeTestimonialPageModule {}
