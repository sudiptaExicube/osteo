import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { MustMatch } from '../validators/password.validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  registrationForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off-outline';
  passwordType2: string = 'password';
  passwordIcon2: string = 'eye-off-outline';
  userType: any = 'Male';
  public has_accepted_gdpr:boolean = true;
  public countryIso:any;

  constructor(private formBuilder: FormBuilder, public platform: Platform, public navController: NavController, public global: GlobalService, private apiService: ApiService) {
		this.registrationForm = this.formBuilder.group({
      lastname: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z\s]*$/)
      ])],
      firstname: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z\s]*$/)
      ])],
      // telephone: ['', [Validators.compose([Validators.pattern(/^([0|\+[0-9]{1,5})?([0-9][0-9]{8})$/), Validators.required])]],
      telephone: [''],
      howfeel: ['', Validators.required],
      howarrive: ['', Validators.required],
      email: ['', [Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])]],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ])],
      cpassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'cpassword')
    });

    console.log("this.global.allCountryList: ",this.global.allCountryList);
    if(this.global.allCountryList.length == 0){
      this.fetchCountrylist();
    }
  }
  ngOnInit() {
  }

  /* == Fetch country list == */
  fetchCountrylist(){
    this.global.presentLoadingDefault();
    this.apiService.fetchCountryList()
      .then((success:any)=>{
        this.global.presentLoadingClose();
          console.log("country success : ", success);
          if(success.status){
            this.global.allCountryList = success.data;
          }
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose();
        this.global.presentToast("Impossible de récupérer le pays. S'il vous plait, vérifiez votre connexion internet")
        console.error(error);
      })
  }

  segmentChanged(ev) {
    console.log(ev.detail.value);
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline';
  }

  hideShowPassword2() {
    this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
    this.passwordIcon2 = this.passwordIcon2 === 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline';
  }

  login() {
    this.navController.navigateRoot(["/login"]);
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  signup() {
    // let randomFcm = Math.floor(Math.random() * 1000000000);
    console.log("this.country : ", this.countryIso);
    if(this.has_accepted_gdpr != true){
      this.global.presentToast("Merci d'accepter les termes et conditions Avant de continuer")
    }else if (!this.countryIso){
      this.global.presentToast("Veuillez d'abord sélectionner le pays")
    }else{
      let dd:number = this.has_accepted_gdpr == true ? 1 : 0
      this.global.presentLoadingDefault();
      this.apiService.registration(this.userType, this.registrationForm.value,dd,this.countryIso).then((data:any) => {
        console.log(data);
        this.global.presentLoadingClose();
        if(data.status) {
          if(data.status ==400){
            if(data.error){
              this.global.presentToast(data.error.message);
            }
          }else if(data.status ==401){
            // this.callTokenExpFunc();
            this.forceLogout();
          }else{
            this.global.presentToast(data.message);
            let navigationExtras: NavigationExtras = {
              state: {
                email: data.data.userdata.email
              }
            };
            this.navController.navigateRoot(["/otp-verification"], navigationExtras);
          }
        }else{
          console.log("error :", data);
          
          if(data.errors){
            if(data.errors.fname){
              this.global.presentToast(data.errors.fname);
            }else if(data.errors.lname){
              this.global.presentToast(data.errors.lname);
            }else if(data.errors.reg_ques_one){
              this.global.presentToast(data.errors.reg_ques_one);
            }else if(data.errors.reg_ques_two){
              this.global.presentToast(data.errors.reg_ques_two);
            }else{
              this.global.presentToast(data.message);
            }
          }else{
            this.global.presentToast(data.message);
          }
        }
      });
    }
    
  }
}
