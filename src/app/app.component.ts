import { Component, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { Platform, NavController, IonRouterOutlet, ToastController, MenuController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { GlobalService } from './services/global.service';
import { NavigationExtras, Router, RouterEvent } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { FeedbackModalHealthPage } from './feedback-modal-health/feedback-modal-health.page';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  backButtonSubscription;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  lastTimeBackPress = Date.now();
  timePeriodToExit = 2000;

  selectedEmoji: any = '';
  socialLink: any = [];

  constructor(public navController: NavController, public statusBar: StatusBar, public global: GlobalService, private menu: MenuController, private platform: Platform, private router: Router, public toastController: ToastController, private nativeStorage: NativeStorage, private iab: InAppBrowser, private apiService: ApiService) {
    this.initializeApp();
    this.backButtonEvent();
  }
  
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#70826A');
      // this.global.setPushNotification();
      this.nativeStorage.getItem('userdetails').then(
        userdata => {
          console.log('Stored user details', userdata);
          if(!userdata) {
            this.navController.navigateRoot('/login');
          } else {
            this.global.isLoggedIn = true;
            this.global.userdetails = userdata;
            // this.global.userdetails.user_token = userdata.token;
            this.global.user_access_token = userdata.token;
            if(!userdata.plan && userdata.plan_activated == 'no')
              this.navController.navigateRoot(["/home"]);
            else
              this.navController.navigateRoot(["/user-type-category"]);
          }
        }, error => {
          this.navController.navigateRoot('/login');
          console.error(error);
        }
      );
      this.getSocialLink();
    });
  }

  goToPremium() {
    this.menu.close();
    if(!this.global.isLoggedIn) {
      this.presentToastForPremium('Connectez-vous pour acheter un abonnement');
      return false;
    }
    this.navController.navigateForward(["/subscriptions"]);
  }

  // async presentToastForPremium(msg) {
  //   const toast = await this.global.toastCtrl.create({
  //     // header: 'Toast header',
  //     message: msg,
  //     position: 'bottom',
  //     color: 'warning',
  //     buttons: [
  //       {
  //         text: 'Annuler',
  //         role: 'cancel',
  //         handler: () => {
  //         }
  //       },
  //       {
  //         text: 'S\'identifier',
  //         // role: 'cancel',
  //         handler: () => {
  //           this.navController.navigateForward('/registration');
  //         }
  //       }
  //     ]
  //   });
  //   await toast.present();

  //   const { role } = await toast.onDidDismiss();
  //   console.log('onDidDismiss resolved with role', role);
  // }

  async presentToastForPremium(msg) {
    const alert = await this.global.alertCtrl.create({
      cssClass: 'premium-popup-custom-class',
      mode: 'ios',
      // header: 'Confirm!',
      message: msg,
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'S\'identifier',
          handler: () => {
            this.navController.navigateForward('/registration');
          }
        }
      ]
    });

    await alert.present();
  }

  goToTestimonial() {
    this.navController.navigateForward(["/welcome-testimonial"]);
  }

  goToProfile() {
    this.navController.navigateForward(["/profile"]);
  }

  goToBlogs() {
    this.navController.navigateForward(["/blogs"]);
  }

  goToHygine() {
    this.navController.navigateForward(["/hygienic-videos"]);
  }

  goToContact() {
    this.navController.navigateForward(["/contact-us"]);
  }

  goToPaymentHistory() {
    this.navController.navigateForward(["/payment-history"]);
  }

  goToCMSPage(page:any) {
    let navigationExtras: NavigationExtras = {
      state: {
        page: page
      }
    };
    this.navController.navigateForward(["/cms-pages"], navigationExtras);
  }

  goToMyHistory() {
    this.navController.navigateForward(["/my-history"]);
  }

  login() {
    this.navController.navigateRoot('/login');
  }

  logout() {
    if(!this.global.userdetails.plan && this.global.userdetails.plan_activated == 'no') {
      this.global.presentConfirm("Se déconnecter", "Êtes-vous sûr de vouloir vous déconnecter?", "Non", "Oui").then(success=>{
        if(success == 2) {
          this.global.isLoggedIn = false;
          this.global.userdetails = {};
          this.global.user_access_token = '';
          this.nativeStorage.setItem('userdetails', null);
          this.navController.navigateRoot('/login');
        }
      });
    }
    else {
      if(this.global.userdetails.logout_popup) {
        this.healthFeedbackModal();
      } else {
        this.global.presentConfirm("Se déconnecter", "Êtes-vous sûr de vouloir vous déconnecter?", "Non", "Oui").then(success=>{
          if(success == 2) {
            this.global.isLoggedIn = false;
            this.global.userdetails = {};
            this.global.user_access_token = '';
            this.nativeStorage.setItem('userdetails', null);
            this.navController.navigateRoot('/login');
          }
        });
      }
    }
  }

  /* Delete Account */
  deleteAccount(){
    this.global.presentConfirm("Supprimer le compte", "Etes-vous sûr ? La suppression d'un compte entraîne la suppression de toutes les données. Aucune donnée ne peut être récupérée par la suite.", "Non", "Oui").then(success=>{
      if(success == 2) {
        this.apiService.deleteAccount()
        .then((data:any) => {
          console.log(data);
          // this.global.presentLoadingClose();
          if(data.status) {
            this.global.presentToast(data.message);
            this.global.isLoggedIn = false;
            this.global.userdetails = {};
            this.global.user_access_token = '';
            this.nativeStorage.setItem('userdetails', null);
            this.navController.navigateRoot('/login');
          } else {
            this.global.presentToast(data.message);
          }
        });  


      }
    });
  }

  backButtonEvent() {
    document.addEventListener("backbutton", () => {
      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        if(outlet && outlet.canGoBack()) {
          outlet.pop();
        } else if(this.router.url === '/login' || this.router.url === '/user-type-category' || this.router.url === '/home' || this.router.url === '/reset-password' || this.router.url === '/otp-verification') {
          if(Date.now() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp();
          } else {
            this.showToast('Appuyez à nouveau pour quitter l\'application');
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      })
    });
  }

  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      // mode: 'ios'
    });
    toast.present();
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  async healthFeedbackModal() {
    const modal = await this.global.modalController.create({
      component: FeedbackModalHealthPage,
      cssClass: 'feedback-modal-health',
      componentProps: {
        'type': 'logout'
      }
    });

    modal.onDidDismiss().then(data => {
      console.log('dismissed', data);
      // this.global.isfeedbackModal = false;
      // this.navController.navigateRoot('/user-type-category');
    });

    return await modal.present();
  }

  getSocialLink() {
    this.apiService.socialLinks().then((data:any) => {
      console.log(data);
      console.log(data.data);
      if(data.status) {
        this.socialLink = data.data;
      }
    });
  }

  openLink(link:any) {
    console.log("Link -", link)
    const options: InAppBrowserOptions = {
      zoom: 'no',
      clearcache:'yes',
      
      
    }
    const browser = this.iab.create(link, '_blank', options);
    browser.show();
    // const browser = this.iab.create(link, '_self', options);

    
  }
}
