import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  videoList: any = [];
  testimonials: any = [];
  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1.2,
    spaceBetween: 20
  };
  platformis: any = '';

  constructor(public menu: MenuController, public navController: NavController, private router: Router, public global: GlobalService, private apiService: ApiService, 
    public platform: Platform,
    private iab: InAppBrowser
  ) {
    this.menu.swipeGesture(true);
    this.platformis = this.platform.is('android') ? 'a' : 'i';
  }

  ngOnInit() {
    this.global.rootpage = 'home';
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  ionViewDidEnter() {
    this.fetchAppVersions();
    this.getFreeVideos();
    this.getTestimonial();
  }

  fetchAppVersions(){
    this.apiService.getAppVersions()
    .then((success:any)=>{
      if(success.status == true){
        if(success.data){
            this.global.android_version = success.data[0].android_version;
            this.global.ios_version = success.data[0].ios_version

            if(success.data[0].check_version == "true"){
              this.fecthVersionHistory();
            }
            // this.fecthVersionHistory();
        }
      }else{
        this.global.presentToast(success.message);
      }
    })
    .catch((error:any)=>{
      console.error(error);
      this.global.presentToast("Unable to fetch app version. Please check your internet connection or Restart the App")
    })
  }

  fecthVersionHistory(){
    this.global.appVersion.getVersionNumber()
    .then((value:any)=>{
      console.log("Version value is : ", value);
      if(this.global.platform.is('ios') == true){
        if(value != this.global.ios_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.ios_app_url)
        }
        
      }else if(this.global.platform.is('android') == true){
        if(value != this.global.android_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.android_app_url)
        }
      }
    })
    .catch((error:any)=>{
      console.log("Version error is : ", error);
    })
  }

  showVersionUpdateAlert(appupdateUrl){
    this.global.versionAlert("Une nouvelle version est disponible. Veuillez mettre à jour l'application maintenant")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        if(this.global.platform.is('android') == true){
          navigator['app'].exitApp();
        }
        // navigator['app'].exitApp();
        // window.open(appupdateUrl,"_system")
        const options: InAppBrowserOptions = { zoom: 'no', clearcache:'yes', }
        const browserOne = this.iab.create(appupdateUrl, '_system', options);
      }
    })
  }




  goToPremium() {
    if(!this.global.isLoggedIn) {
      this.presentToastForPremium('Connectez-vous pour acheter un abonnement');
      return false;
    }
    this.navController.navigateForward(["/subscriptions"]);
  }

  // async presentToastWithOptions(msg) {
  //   const toast = await this.global.toastCtrl.create({
  //     // header: 'Toast header',
  //     message: msg,
  //     position: 'bottom',
  //     color: 'warning',
  //     buttons: [
  //       {
  //         text: 'Annuler',
  //         role: 'cancel',
  //         handler: () => {
  //         }
  //       },
  //       {
  //         text: 'S\'identifier',
  //         // role: 'cancel',
  //         handler: () => {
  //           this.navController.navigateForward('/registration');
  //         }
  //       }
  //     ]
  //   });
  //   await toast.present();

  //   const { role } = await toast.onDidDismiss();
  //   console.log('onDidDismiss resolved with role', role);
  // }

  async presentToastForPremium(msg) {
    const alert = await this.global.alertCtrl.create({
      cssClass: 'premium-popup-custom-class',
      mode: 'ios',
      // header: 'Confirm!',
      message: msg,
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'S\'identifier',
          handler: () => {
            this.navController.navigateForward('/registration');
          }
        }
      ]
    });

    await alert.present();
  }

  getFreeVideos() {
    this.global.presentLoadingDefault();
    this.apiService.getFreeVideos().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.videoList = data.data;
        }

      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose();
    })
  }

  getTestimonial() {
    this.apiService.testimonial().then((data:any) => {
      console.log(data);
      if(data.status) {
        this.testimonials = data.data;
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose();
    })
  }
}
