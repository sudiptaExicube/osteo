import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { Router, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.page.html',
  styleUrls: ['./otp-verification.page.scss'],
})
export class OtpVerificationPage implements OnInit {
  otp:any = {
    input1:"", input2:"", input3:"", input4:""
  };
  email: any = '';
  givenOtp: any;

  constructor(public global: GlobalService, private apiService: ApiService, private router: Router, private navController: NavController) {
    this.email = this.router.getCurrentNavigation().extras.state.email;
  }

  ngOnInit() {
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  moveFocus(next:any, event:any, prev:any){
    // console.log(event, prev);
    if(event.key == 'Backspace') {
      prev.setFocus();
    } else {
      next.setFocus();
    }
  }

  verify() {
    this.givenOtp = this.otp.input1+this.otp.input2+this.otp.input3+this.otp.input4;
    // console.log(this.givenOtp);
    if(!this.givenOtp) {
      this.global.presentToast('Le code de vérification ne peut pas être vide');
    } else if(this.givenOtp.length != 4) {
      this.global.presentToast("La longueur OTP doit être de 4 chiffres");
    } else {
      this.global.presentLoadingDefault();
      this.apiService.verifyOTP(this.email, this.givenOtp).then((data:any) => {
        console.log(data);
        this.global.presentLoadingClose();
        this.global.presentToast(data.message);
        if(data.status) {
          if(data.status ==400){
            if(data.error){
              this.global.presentToast(data.error.message);
            }
          }else if(data.status ==401){
            // this.callTokenExpFunc();
            this.forceLogout();
          }else{
            this.navController.navigateRoot(["/login"]);
          }
        }
      });
    }
  }

  resendOtp() {
    this.global.presentLoadingDefault();
    this.apiService.resendOTP(this.email).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      this.global.presentToast(data.message);
    });
  }
}
