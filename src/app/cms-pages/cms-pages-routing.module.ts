import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CmsPagesPage } from './cms-pages.page';

const routes: Routes = [
  {
    path: '',
    component: CmsPagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CmsPagesPageRoutingModule {}
