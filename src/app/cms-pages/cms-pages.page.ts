import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-cms-pages',
  templateUrl: './cms-pages.page.html',
  styleUrls: ['./cms-pages.page.scss'],
})
export class CmsPagesPage implements OnInit {
  page: any = '';
  pageData: any = {};
  
  

  constructor( public navController:NavController,
    private router: Router, public global: GlobalService, 
    private apiService: ApiService,
    public sanitizer:DomSanitizer
    ) {
    this.page = this.router.getCurrentNavigation().extras.state.page;
  }

  ngOnInit() {
    this.getPageData();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }
  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  getPageData() {
    this.global.presentLoadingDefault();
    this.apiService.getCMSPageData(this.page).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.pageData = data.data;
          if(this.pageData.page_content){
            this.pageData.page_content = this.sanitizer.bypassSecurityTrustHtml(this.pageData.page_content)
          }
        }
      }
    });
  }
}
