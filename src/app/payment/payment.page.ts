import { Component, NgZone, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { Stripe } from '@ionic-native/stripe/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Router, NavigationExtras } from '@angular/router';
import { IAPProduct, InAppPurchase2 } from '@awesome-cordova-plugins/in-app-purchase-2/ngx';

// declare var ApplePay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  stripeForm: FormGroup;
  planid: any = '';
  planprice:any;
  planname:any="";
  ios_plan_name:any="";
  

  constructor(public global: GlobalService, private apiService: ApiService, private stripe: Stripe, private formBuilder: FormBuilder, private navController: NavController, private nativeStorage: NativeStorage, private zone: NgZone, private router: Router,
    public store:InAppPurchase2
    
    ) {
    this.planid = this.router.getCurrentNavigation().extras.state.planid;
    this.planprice = this.router.getCurrentNavigation().extras.state.planprice;
    this.planname = this.router.getCurrentNavigation().extras.state.name;
    this.ios_plan_name = this.router.getCurrentNavigation().extras.state.ios_plan_name;
    console.log("ios_plan_name :", this.ios_plan_name);
		this.stripeForm = this.formBuilder.group({
      name: ['', Validators.required],
      cardno: ['', Validators.compose([
        Validators.required,
        Validators.minLength(15),
        Validators.maxLength(16)
      ])],
      // email: ['', [Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])]],
      monthDate: ['', Validators.required],
      cvv: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(3)
      ])],
    });

    // if(this.ios_plan_name != ""){
    //   this.configureStart()
    // }else{
    //   this.checkIosPlanName();
    // }

  }


  // public checkIosPlanName(){
  //   if(this.ios_plan_name != ""){
  //     this.configureStart()
  //   }else{
  //     this.ios_plan_name = this.router.getCurrentNavigation().extras.state.ios_plan_name;
  //     this.checkIosPlanName();
  //   }
  // }

  /* ========= Inapp purchases configuration function ========= */
  // public configureStart(){
  //   this.global.platform.ready()
  //   .then(()=>{
  //     // console.log(" .... Platform is Ready now ......");
  //       // this.store.verbosity = this.store.DEBUG;
  //       this.store.register({
  //         // id: "ponctuel_10_1m",
  //         id:this.ios_plan_name,
  //         type: this.store.NON_RENEWING_SUBSCRIPTION,
  //       });
  //       // this.store.when("ponctuel_10_1m")
  //       this.store.when(this.ios_plan_name)
  //         .approved(p => {
  //           this.global.dynamictimeToastHide();
  //           console.log("p.approved : ",JSON.stringify(p));
  //           p.verify()
  //         })
  //         .verified(p => {
  //           console.log("p.verified : ",JSON.stringify(p));
  //           p.finish()
  //         })
  //         .finished(p =>{
  //           // alert("Order finished : "+ JSON.stringify(p));
  //           if(p.transaction){
  //             if(p.transaction.id){
  //               // alert("Order finished : "+ JSON.stringify(p.transaction.id));
  //               // this.navController.navigateRoot(["/user-type-category"]);
  //               this.startSubmittingResponseData_forAppleInappPurchases(p)
  //             }
  //           }
  //         })
  //         .cancelled(p=>{
  //           this.global.dynamictimeToastHide();
  //           //message == > Payment canceled by the user
  //           // alert("Paiement annulé par l'utilisateur")
  //           this.global.presentToast("Paiement annulé par l'utilisateur")
  //         });
  //       this.store.refresh();

  //   })
  //   .catch((error)=>{
  //     console.log(" .... Platform is not ready");
  //   })
  // }

  /* ========= Inapp purchases date store to database ========= */
  // startSubmittingResponseData_forAppleInappPurchases(inApppurchases_response){
  //   this.global.presentLoadingDefault();
  //   let usertoken = this.global.userdetails.user_token;
  //   this.apiService.submitInappPurchasesResponse(this.planid, inApppurchases_response)
  //   .then((data: any) => {
  //     console.log("completePayment API data : ",data);
  //     // this.global.presentToast(data.message);
  //     this.global.presentLoadingClose();
  //     if(data.status) {
  //       if(data.status ==400){
  //         if(data.error){
  //           this.global.presentToast(data.error.message);
  //         }
  //       }else{
  //         this.global.presentToast(data.message);
  //         if(data.data.payment_status) {
  //           this.zone.run(() => {
  //             this.global.isLoggedIn = true;
  //             this.global.userdetails = data.data;
  //             this.global.userdetails.user_token = usertoken;
  //             this.global.user_access_token = usertoken;
  //             this.nativeStorage.setItem('userdetails', this.global.userdetails)
  //             .then((data) => {
  //               console.log('Stored item!', data);
  //               this.navController.navigateRoot(["/user-type-category"]);
  //             },
  //             error => console.error('Error storing item', error)
  //             );
  //           });
  //         }
  //       }
  //     }else{
  //       if(data.message){
  //         this.global.presentToast(data.message);
  //       }
  //     }
  //   })
  //   .catch((error)=>{
  //     this.global.presentLoadingClose();
  //     console.log("completePayment Error : ", error);
  //   })

  // }


  ngOnInit() {
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  /* ========= Inapp purchases ========= */
  // applePayButton(){
    
  //   this.global.dynamictimeToastshow("Traitement en cours, veuillez patienter...")
  //   this.store.order(this.ios_plan_name)
  //   .then(success=>{
  //     console.log("order success  : ", JSON.stringify(success))
  //   })
  //   .catch(error=>{
  //     this.global.dynamictimeToastHide();
  //     console.log("Order error : ", JSON.stringify(error));
  //   })
  // }
  
  /* ========= Stripe payment initiate functionc (Cretae stripe card token) ========= */
  payment() {
    let fulldate = this.stripeForm.value.monthDate.split('T')[0];
    let year = fulldate.split('-')[0];
    let month = fulldate.split('-')[1];
    this.global.presentLoadingDefault();
    // this.stripe.setPublishableKey('pk_test_iYW7JLszxcGWBW6ZaDLqDFqj');
    this.stripe.setPublishableKey('pk_live_51I2Gd8EHwd1D7YltumWfUTkKNQuXFuJUugpG0uG4UTEVjKmkqIZPVRzdnbm9d76YhQP5H8MmrGJhGv076T3foA5u00qnDrn3AE');
    let card = {
      number: this.stripeForm.value.cardno,
      expMonth: month,
      expYear: year,
      cvc: this.stripeForm.value.cvv,
      name: this.stripeForm.value.name,
      currency: 'EUR'
    }
    this.stripe.createCardToken(card)
    .then(token => {
      console.log(token);
      this.completePayment(token.id);
    })
    .catch(error => {
      console.log(error);
      this.global.presentLoadingClose();
      this.global.presentToast(error);
    });
  }

  /* ========= Stripe payment (complete and store data to database) ========= */
  completePayment(token: any) {
    let usertoken = this.global.userdetails.user_token;
    this.apiService.payment(this.planid, token).then((data: any) => {
      console.log("completePayment API data : ",data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.global.presentToast(data.message);
          if(data.data.payment_status) {
            this.zone.run(() => {
              this.global.isLoggedIn = true;
              this.global.userdetails = data.data;
              this.global.userdetails.user_token = usertoken;
              this.global.user_access_token = usertoken;
              this.nativeStorage.setItem('userdetails', this.global.userdetails)
              .then((data) => {
                console.log('Stored item!', data);
                this.navController.navigateRoot(["/user-type-category"]);
              },
              error => console.error('Error storing item', error)
              );
            });
          }
        }
      }else{
        if(data.message){
          this.global.presentToast(data.message);
        }
      }
    })
    .catch((error)=>{
      console.log("completePayment Error : ", error);
    })
  }



  /* == NOT ADDED IN CURRENT VERSION (APPLE PAY ) ==== */

  /* == FOR apple pay stripe payment ==== */
  // completeIOSPayment(token: any){
  //   let usertoken = this.global.userdetails.user_token;
  //   this.apiService.payment(this.planid, token).then((data: any) => {
  //     console.log("completePayment API data : ",data);
  //     this.global.presentToast(data.message);
  //     this.global.presentLoadingClose();
  //     if(data.status) {
  //       if(data.status ==400){
  //         if(data.error){
  //           this.global.presentToast(data.error.message);
  //         }
  //       }else{
  //         if(data.data.payment_status) {
  //           ApplePay.completeLastTransaction('success');
  //           this.global.presentToast("Paiement réussi");
  //           this.zone.run(() => {
  //             this.global.isLoggedIn = true;
  //             this.global.userdetails = data.data;
  //             this.global.userdetails.user_token = usertoken;
  //             this.global.user_access_token = usertoken;
  //             this.nativeStorage.setItem('userdetails', this.global.userdetails)
  //             .then((data) => {
  //               console.log('Stored item!', data);
  //               this.navController.navigateRoot(["/user-type-category"]);
  //             },
  //             error => console.error('Error storing item', error)
  //             );
  //           });
  //         }
  //       }
  //     }else{
  //       ApplePay.completeLastTransaction('failure');
  //     }
  //   })
  //   .catch((error)=>{
  //     console.log("completePayment Error : ", error);
  //     ApplePay.completeLastTransaction('failure');
  //   })
  // }

   /* == NOT ADDED IN CURRENT VERSION (APPLE PAY ) ==== */

  
}
