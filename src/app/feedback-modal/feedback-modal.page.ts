import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.page.html',
  styleUrls: ['./feedback-modal.page.scss'],
})
export class FeedbackModalPage implements OnInit {
  selectedEmoji: any = '';
  selectedEmoji2: any = '';
  @Input() categoryType: any;
  @Input() vid: any;
  comment: any = '';
  comment2: any = '';
  firstQues: boolean = true;

  constructor(private navController: NavController, public global: GlobalService, private apiService: ApiService) {
  }

  ngOnInit() {
    console.log(this.categoryType);
    console.log(this.vid);
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  playAgain() {
    console.log('play again');
    this.global.modalController.dismiss({
      'dismissed': true
    });
    this.navController.navigateRoot('/user-type-category');
  }

  selectFeedback(selected:any) {
    this.selectedEmoji = selected;
  }

  selectFeedback2(selected:any) {
    this.selectedEmoji2 = selected;
  }

  sendFeedback() {
    if(!this.selectedEmoji2) {
      this.global.presentToast('Veuillez sélectionner un emoji');
      return false;
    }
    if(!this.comment2) {
      this.global.presentToast('Entrez vos commentaires');
      return false;
    }
    this.global.presentLoadingDefault();
    this.apiService.sendVideoFeedback(this.selectedEmoji, this.comment, this.selectedEmoji2, this.comment2, this.vid, this.categoryType).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      this.global.presentToast(data.message);
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.global.modalController.dismiss({
            'dismissed': true
          });
          this.navController.navigateRoot('/user-type-category');
        }


      }
    })
  }

  nextQues() {
    if(!this.selectedEmoji) {
      this.global.presentToast('Veuillez sélectionner un emoji');
      return false;
    }
    if(!this.comment) {
      this.global.presentToast('Entrez vos commentaires');
      return false;
    }
    this.firstQues = false;
  }
}
