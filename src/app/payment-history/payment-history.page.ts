import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.page.html',
  styleUrls: ['./payment-history.page.scss'],
})
export class PaymentHistoryPage implements OnInit {
  paymentHistory: any = [];
  isData: boolean = true;

  constructor(public navController:NavController, public global: GlobalService, private apiService: ApiService, private iab: InAppBrowser) { }

  ngOnInit() {
    this.getPaymentHistory();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  getPaymentHistory() {
    this.global.presentLoadingDefault();
    this.apiService.paymentHistory().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.paymentHistory = data.data;
          this.isData = data.data.length > 0 ? true : false;
        }
      }
    });
  }

  // getInvoice(url: string) {
  //   window.open(url);
  //   console.log(url);
  // }

  getInvoice(link:any) {
    const options: InAppBrowserOptions = {
      zoom: 'no'
    }
    const browser = this.iab.create(link, '_system', options);
  }
}
