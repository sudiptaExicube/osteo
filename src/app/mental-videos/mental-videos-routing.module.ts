import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MentalVideosPage } from './mental-videos.page';

const routes: Routes = [
  {
    path: '',
    component: MentalVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MentalVideosPageRoutingModule {}
