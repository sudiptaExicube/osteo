import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MentalVideosPageRoutingModule } from './mental-videos-routing.module';

import { MentalVideosPage } from './mental-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MentalVideosPageRoutingModule
  ],
  declarations: [MentalVideosPage]
})
export class MentalVideosPageModule {}
