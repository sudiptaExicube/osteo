import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { ApiService } from '../services/api.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-mental-videos',
  templateUrl: './mental-videos.page.html',
  styleUrls: ['./mental-videos.page.scss'],
})
export class MentalVideosPage implements OnInit {
  category: any = {};
  mentalVideos: any = [];
  isVideos: boolean = true;

  constructor(private navController: NavController, private router: Router, public global: GlobalService, private apiService: ApiService) {
    this.category = this.router.getCurrentNavigation().extras.state.category;
  }

  ngOnInit() {
    this.getChildVideos();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  getChildVideos() {
    this.global.presentLoadingDefault();
    this.apiService.mentalVideos(this.category.id).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.mentalVideos = data.data;
          this.isVideos = this.mentalVideos.length > 0 ? true : false;
        }
      }
    })
  }

  goToDetails(item:any) {
    let navigationExtras: NavigationExtras = {
      state: {
        detail: item,
        categoryType: 'mental'
      }
    };
    this.navController.navigateForward(["/video-and-details"], navigationExtras);
  }
}
