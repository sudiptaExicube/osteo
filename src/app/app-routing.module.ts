import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'login',
  //   pathMatch: 'full'
  // },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'subscriptions',
    loadChildren: () => import('./subscriptions/subscriptions.module').then( m => m.SubscriptionsPageModule)
  },
  {
    path: 'user-type-category',
    loadChildren: () => import('./user-type-category/user-type-category.module').then( m => m.UserTypeCategoryPageModule)
  },
  {
    path: 'problem-category',
    loadChildren: () => import('./problem-category/problem-category.module').then( m => m.ProblemCategoryPageModule)
  },
  {
    path: 'human-body',
    loadChildren: () => import('./human-body/human-body.module').then( m => m.HumanBodyPageModule)
  },
  {
    path: 'mental-categories',
    loadChildren: () => import('./mental-categories/mental-categories.module').then( m => m.MentalCategoriesPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'otp-verification',
    loadChildren: () => import('./otp-verification/otp-verification.module').then( m => m.OtpVerificationPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'blogs',
    loadChildren: () => import('./blogs/blogs.module').then( m => m.BlogsPageModule)
  },
  {
    path: 'child-videos',
    loadChildren: () => import('./child-videos/child-videos.module').then( m => m.ChildVideosPageModule)
  },
  {
    path: 'mental-videos',
    loadChildren: () => import('./mental-videos/mental-videos.module').then( m => m.MentalVideosPageModule)
  },
  {
    path: 'hygienic-videos',
    loadChildren: () => import('./hygienic-videos/hygienic-videos.module').then( m => m.HygienicVideosPageModule)
  },
  {
    path: 'body-parts-videos',
    loadChildren: () => import('./body-parts-videos/body-parts-videos.module').then( m => m.BodyPartsVideosPageModule)
  },
  {
    path: 'video-and-details',
    loadChildren: () => import('./video-and-details/video-and-details.module').then( m => m.VideoAndDetailsPageModule)
  },
  {
    path: 'feedback-modal',
    loadChildren: () => import('./feedback-modal/feedback-modal.module').then( m => m.FeedbackModalPageModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'cms-pages',
    loadChildren: () => import('./cms-pages/cms-pages.module').then( m => m.CmsPagesPageModule)
  },
  {
    path: 'my-history',
    loadChildren: () => import('./my-history/my-history.module').then( m => m.MyHistoryPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'feedback-modal-health',
    loadChildren: () => import('./feedback-modal-health/feedback-modal-health.module').then( m => m.FeedbackModalHealthPageModule)
  },
  {
    path: 'welcome-testimonial',
    loadChildren: () => import('./welcome-testimonial/welcome-testimonial.module').then( m => m.WelcomeTestimonialPageModule)
  },
  {
    path: 'payment-history',
    loadChildren: () => import('./payment-history/payment-history.module').then( m => m.PaymentHistoryPageModule)
  },
  {
    path: 'blog-video',
    loadChildren: () => import('./blog-video/blog-video.module').then( m => m.BlogVideoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
