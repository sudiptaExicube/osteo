import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { NavigationExtras } from '@angular/router';
import { FeedbackModalHealthPage } from '../feedback-modal-health/feedback-modal-health.page';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off-outline';

  constructor(private formBuilder: FormBuilder, public platform: Platform, public navController: NavController, public menu: MenuController, private apiService:  ApiService, public global: GlobalService, private nativeStorage: NativeStorage,private iab: InAppBrowser) {
    this.menu.swipeGesture(false);
		this.loginForm = this.formBuilder.group({
      // email: ['sankarnandi1010@gmail.com', Validators.required],
      // email: ['john@yopmail.com', Validators.required],
      // email: ['test1@yopmail.com', Validators.required],
      // password: ['123456', Validators.required]

      // email: ['sbwri22@yopmail.com', Validators.required],
      // email: ['pradip1@yopmail.com', Validators.required],
      // password: ['12345678', Validators.required]
      
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    
    // ==> fetch countries
    this.fetchAppVersions();
    // this.fetchCountry();


    
  }

  fetchAppVersions(){
    this.apiService.getAppVersions()
    .then((success:any)=>{
      if(success.status == true){
        if(success.data){
            this.global.android_version = success.data[0].android_version;
            this.global.ios_version = success.data[0].ios_version
            
            if(success.data[0].check_version == "true"){
              this.fecthVersionHistory();
            }
            // this.fecthVersionHistory();
        }
      }else{
        this.global.presentToast(success.message);
      }
    })
    .catch((error:any)=>{
      console.error(error);
      this.global.presentToast("Unable to fetch app version. Please check your internet connection or Restart the App")
    })
  }

  fecthVersionHistory(){
    console.log("Working... version check")
    this.global.appVersion.getVersionNumber()
    .then((value:any)=>{
      console.log("Version value is : ", value);
      if(this.global.platform.is('ios') == true){
        if(value != this.global.ios_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.ios_app_url)
        }
        
      }else if(this.global.platform.is('android') == true){
        if(value != this.global.android_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.android_app_url)
        }
      }
    })
    .catch((error:any)=>{
      console.log("Version error is : ", error);
    })
  }

  showVersionUpdateAlert(appupdateUrl){
    this.global.versionAlert("Une nouvelle version est disponible. Veuillez mettre à jour l'application maintenant")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        if(this.global.platform.is('android') == true){
          navigator['app'].exitApp();
        }
        // navigator['app'].exitApp();
        // window.open(appupdateUrl,"_system")
        const options: InAppBrowserOptions = { zoom: 'no', clearcache:'yes', }
        const browserOne = this.iab.create(appupdateUrl, '_system', options);
      }
    })
  }


  ngOnInit() {
  }

  /* == fetch countries == */
  fetchCountry(){
    this.apiService.fetchCountryList()
      .then((success:any)=>{
          console.log("country success : ", success);
          if(success.status){
            this.global.allCountryList = success.data;
          }
      })
      .catch((error:any)=>{
        console.error(error);
      })
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline';
  }

  login() {
    this.global.presentLoadingDefault();
    this.apiService.login(this.loginForm.value.email, this.loginForm.value.password).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.data.status == 'I') {
          this.global.presentToast('Vérifiez votre email pour vous connecter');
          this.apiService.resendOTP(data.data.email).then((data:any) => {
            console.log(data);
          });
          let navigationExtras: NavigationExtras = {
            state: {
              email: data.data.email
            }
          };
          this.navController.navigateRoot(["/otp-verification"], navigationExtras);
          return false;
        }
        this.global.presentToast(data.message);
        this.global.isLoggedIn = true;
        this.global.userdetails = data.data;
        this.global.userdetails.user_token = data.token;
        this.global.user_access_token = data.token;
        this.nativeStorage.setItem('userdetails', this.global.userdetails)
        .then(
          (data) => console.log('Stored item!', data),
          error => console.error('Error storing item', error)
        );
        if(!data.data.plan && data.data.plan_activated == 'no')
          this.navController.navigateRoot(["/home"]);
        else {
          if(data.data.login_popup) {
            this.navController.navigateRoot(["/user-type-category"]).then(() => {
              console.log('root page set');
              this.healthFeedbackModal();
            });
          } else {
            this.navController.navigateRoot(["/user-type-category"]);
          }
        }
      } else {
        this.global.presentToast(data.message);
      }
    });  
  }

  visit() {
    this.global.isLoggedIn = false;
    this.navController.navigateRoot(["/home"]);
  }

  forgetPassword() {
    this.navController.navigateForward(["/forgot-password"]);
  }

  async healthFeedbackModal() {
    const modal = await this.global.modalController.create({
      component: FeedbackModalHealthPage,
      cssClass: 'feedback-modal-health',
      componentProps: {
        'type': 'login'
      }
    });

    modal.onDidDismiss().then(data => {
      console.log('dismissed', data);
      // this.global.isfeedbackModal = false;
      // this.navController.navigateRoot('/user-type-category');
    });

    return await modal.present();
  }
}
