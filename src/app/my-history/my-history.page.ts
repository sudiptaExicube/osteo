import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-my-history',
  templateUrl: './my-history.page.html',
  styleUrls: ['./my-history.page.scss'],
})
export class MyHistoryPage implements OnInit {
  feedbackHistory: any = [];
  isData: boolean = true;
  hideenIndx: number;

  constructor(public global: GlobalService, private apiService: ApiService,public navController:NavController) { }

  ngOnInit() {
    this.getFeedbackHistory();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  openComments(indx: any) {
    console.log(this.hideenIndx, indx);
    this.hideenIndx = indx;
  }

  getFeedbackHistory() {
    this.global.presentLoadingDefault();
    this.apiService.myFeedbackHistory().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.feedbackHistory = data.data;
          this.isData = data.data.length > 0 ? true : false;
        }
      }
    });
  }
}
