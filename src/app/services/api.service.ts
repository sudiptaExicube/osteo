import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  uri = 'https://osteoenpoche.shopconik.com/api/v1';     //OLD URL
  // uri = 'https://osteoenpoche.fr/api/v1'
  

  constructor(public http: HttpClient, public global: GlobalService) { }


  /* === Fetch Country lish ==== */
  fetchCountryList(){
    return new Promise(resolve => {
      this.http.get(`${this.uri}/countries`, {}).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  /* === GET APP VERSIONS === */
  getAppVersions(){
    return new Promise(resolve => {
      this.http.get(`${this.uri}/get-version`, {}).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  getAppVersionsWithuserId(userId){
    return new Promise(resolve => {
      this.http.get(`${this.uri}/get-version?user_id=`+userId, {}).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //============================== Login ==========================//
  login(userid:any, password:any) {
    // console.log('userid => ',userid, ' password => ', password);
    let fd = new FormData();
    fd.append("email_or_phone", userid);
    fd.append("password", password);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/login`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  // Delete Account
  deleteAccount(){
    // let httpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': this.global.userdetails.user_token
    //   })
    // };
    let fd = new FormData();
    fd.append("email", this.global.userdetails.email);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/delete-profile`, fd, {}).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });

  }

  //============================== Registration ==========================//

  registration(usertype:any, userdata:any,has_accepted_gdpr:any,countryIso:any) {
    let fd = new FormData();
    fd.append("gender", usertype);
    fd.append("lname", userdata.lastname);
    fd.append("fname", userdata.firstname);
    fd.append("email", userdata.email);
    fd.append("phone", userdata.telephone);
    fd.append("reg_ques_one", userdata.howfeel);
    fd.append("reg_ques_two", userdata.howarrive);
    fd.append("password", userdata.password);
    fd.append("cpassword", userdata.cpassword);
    fd.append("fcmToken", this.global.deviceToken);
    fd.append("has_accepted_gdpr", has_accepted_gdpr);
    fd.append("country", countryIso ? countryIso :'FR');
    
    

    return new Promise(resolve => {
      this.http.post(`${this.uri}/register`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== OTP Verify =====================//

  verifyOTP(email:any, otp:any) {
    let fd = new FormData();
    fd.append("email", email);
    fd.append("otp", otp);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/verifyotp`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Forgot Password =====================//

  forgotPassword(email:any) {
    let fd = new FormData();
    fd.append("email", email);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/forgotpass`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Reset Password =====================//

  resetPassword(email:any, userdata:any) {
    let fd = new FormData();
    fd.append("email", email);
    fd.append("otp", userdata.otp);
    fd.append("password", userdata.password);
    fd.append("cpassword", userdata.cpassword);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/resetpassword`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Resend OTP =====================//

  resendOTP(email:any) {
    let fd = new FormData();
    fd.append("email", email);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/sendotp`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Free videos =====================//

  getFreeVideos() {
    return new Promise(resolve => {
      this.http.get(`${this.uri}/free-videos`).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Subscription Plans =====================//

  subscriptionPlans() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/subscription-plans`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Issues List =====================//

  issuesList() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/issues`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Blog List =====================//

  blogList() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/blogs`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Child video list =====================//

  childVideoList() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/child-videos`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Mental Categories =====================//

  mentalCategories() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/mental-categories`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Mental Videos =====================//

  mentalVideos(categoryid:any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    let fd = new FormData();
    fd.append("category_id", categoryid);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/mental-videos`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Hygiene videos =====================//

  hygieneVideos() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/hygienic-videos`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Valeria body parts =====================//

  valeriabodyParts() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}/valeria-parts`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Valeria body part videos =====================//

  valeriabodyPartVideos() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("valeria_id", this.global.bodyPartSelectedId);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/valeria-videos`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Update Profile =====================//

  profileUpdate(userdetails:any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("fname", userdetails.firstname);
    fd.append("lname", userdetails.lastname);
    fd.append("phone", userdetails.telephone);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/update-profile`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Contact Form =====================//

  contactForm(formdata:any) {
    let fd = new FormData();
    fd.append("name", formdata.name);
    fd.append("email", formdata.email);
    fd.append("phone", formdata.telephone);
    fd.append("message", formdata.message);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/contact-us-form`, fd).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== CMS Pages =====================//

  getCMSPageData(apiname: any) {
    return new Promise(resolve => {
      this.http.get(`${this.uri}/page/${apiname}`).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Video feedback =====================//

  sendVideoFeedback(emoji1:any, comment1:any, emoji2:any, comment2:any, videoid:any, type:any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("type", type);
    fd.append("video_id", videoid);
    fd.append("icon1", emoji1);
    fd.append("comment1", comment1);
    fd.append("icon2", emoji2);
    fd.append("comment2", comment2);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/save-feedback`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== My History =====================//

  myFeedbackHistory() {
    console.log(this.global.userdetails.user_token);
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };
    console.log(httpOptions.headers)
    return new Promise(resolve => {
      this.http.post(`${this.uri}/my-feedback`, {}, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  sendHealthFeedback(healthRating: any, type:any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("icon", healthRating);
    fd.append("type", type);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/save-app-feedback`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  socialLinks() {
    return new Promise(resolve => {
      this.http.get(`${this.uri}/social-media-links`).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Testimonial =====================//

  testimonial() {
    return new Promise(resolve => {
      this.http.get(`${this.uri}/testimonial`).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Payment =====================//

  payment(planid: any, token: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("plan_id", planid);
    fd.append("_token", token);

    return new Promise(resolve => {
      this.http.post(`${this.uri}/purchase-plan`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }


  // ============= InApp Purchases =======================//
  // submitInappPurchasesResponse(this.planid, inApppurchases_response)
  submitInappPurchasesResponse(planid: any, inApppurchases_response: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    let fd = new FormData();
    fd.append("plan_id", planid);
    fd.append("txn_id", inApppurchases_response.transaction.id);
    fd.append("txn_status", 'success');
    fd.append("transaction_data", JSON.stringify(inApppurchases_response));



//'plan_id=[PLAN ID]
// 'user_id=[USER ID]
// 'txn_id="IOS Txn ID"
// 'txn_status="success" or "fail"
// 'transaction_data'='IOS Transction response'

    return new Promise(resolve => {
      this.http.post(`${this.uri}/purchase-plan-ios`, fd, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }

  //========================== Payment History =====================//

  paymentHistory() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.global.userdetails.user_token
      })
    };

    return new Promise(resolve => {
      this.http.get(`${this.uri}/transactions/list`, httpOptions).subscribe(data => {
        // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve("Une erreur s'est produite. Veuillez réessayer");
      });
    });
  }
}
