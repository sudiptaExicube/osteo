import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, AlertController, LoadingController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
// import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
	isLoggedIn: boolean = false;
  rootpage: any = '';
  bodyPartSelected: any = '';
	bodyPartSelectedId: any = '';
	loading:any;
	userdetails:any = {};
	user_access_token:any = "";
  deviceToken: any = '';
	allCountryList:any=[];
	// isfeedbackModal: boolean = false;

	android_version:any='';
	ios_version:any='';

	public dynamictoast:any;

	public ios_app_url:any="https://apps.apple.com/in/app/lost%C3%A9o-en-poche/id1633483874";
	public android_app_url:any="https://play.google.com/store/apps/details?id=com.rastventure.osteo";

  constructor(
    public actionSheetCtrl: ActionSheetController,
		public modalCtrl: ModalController,
		public platform: Platform,
		public alertCtrl: AlertController, 
		public loadingCtrl: LoadingController, 
		// public storage: Storage,
		public toastCtrl: ToastController,
		public menu: MenuController,
		public navCtrl: NavController,
		public popoverController: PopoverController,
		public sanitizer: DomSanitizer,
		public modalController: ModalController,
		public appVersion:AppVersion,
		// public oneSignal: OneSignal
    public nativeStorage:NativeStorage
  ) { }

	async presentLoadingDefault() {
		this.loading = await this.loadingCtrl.create({
			message: '',
			cssClass: 'loader-waiting'
		});
		await this.loading.present();
	}
	
	presentLoadingClose() {
		this.loading.dismiss();
	}

  changeSanitizerHtml(value){
		return this.sanitizer.bypassSecurityTrustHtml(value);		
  }

	async presentToast(txt:any) {
    const toast = await this.toastCtrl.create({
      message: txt,
      duration: 3000,
			position: 'bottom'
    });
    toast.present();
  }

	async dynamictimeToastshow(txt:any){
		this.dynamictoast = await this.toastCtrl.create({
      message: txt,
			position: 'bottom'
    });
    this.dynamictoast.present();
	}
	async dynamictimeToastHide(){
		this.dynamictoast.dismiss();
	}

	async versionAlert(msg: any): Promise<any> {
    return new Promise((success) => {
      this.alertCtrl.create({
        cssClass: 'my-custom-class',
        mode: 'ios',
        header:"Update",
        message: msg,
        backdropDismiss: false,
        buttons: [
          // {
          //   text: 'Cancel',
          //   handler: () => {
          //     success(1);
          //   }
          // },
          {
            text: 'Mise à jour',
            handler: () => {
              success(2);
            }
          }
        ]
      })
        .then(alert => {
          // Now we just need to present the alert
          alert.present();
        });
    });
  }

  async tokenExpireAlert(msg: any): Promise<any> {
    return new Promise((success) => {
      this.alertCtrl.create({
        cssClass: 'my-custom-class',
        mode: 'ios',
        header:"Alert",
        message: msg,
        backdropDismiss: false,
        buttons: [
          {
            text: "D'accord",
            handler: () => {
              success(2);
            }
          }
        ]
      })
        .then(alert => {
          // Now we just need to present the alert
          alert.present();
        });
    });
  }

	presentConfirm(alertTitle:any, alertMsg:any, button1:any, button2:any):Promise<any> {
		return new Promise((success)=>{
			this.alertCtrl.create({
			header: alertTitle,
			message: alertMsg,
			mode: "ios",
			buttons: [
				{
					text: button1,
					role: 'Cancelar',
					handler: () => {
						success(1);
					}
				},
				{
					text: button2,
					handler: () => {
						success(2);
					}
				}
			]
			})
      .then(alert => {
        // Now we just need to present the alert
        alert.present();
      });
		});
	}

	errorresponse(responseobject:any){
		this.presentToast(responseobject.message);
	}

	getSafeUrl(url) {
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);		
	}
	
	tomorrow() {
    let today = new Date();
    let todayDate = ('0'+(today.getDate() + 1)).slice(-2);
    let todayMonth = ('0'+(today.getMonth()+1)).slice(-2);
    let year = today.getFullYear();
    let mindate = year+'-'+todayMonth+'-'+todayDate;
    return(mindate);
	}

	maxday() {
    let today = new Date();
    let todayDate = ('0'+(today.getDate() + 1)).slice(-2);
    let todayMonth = ('0'+(today.getMonth()+1)).slice(-2);
    let lstyear = today.getFullYear()+10;
    let maxdate = lstyear+'-'+todayMonth+'-'+todayDate;
    return(maxdate);
  }

  // setPushNotification() {
  //   console.log('inside set push notification');
  //   this.oneSignal.startInit('c51ec1e0-0ae3-466c-8325-d66f774fce9e', '932040908003');
  //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
  //   this.oneSignal.handleNotificationReceived().subscribe((notificationData) => {
  //     // do something when notification is received
  //     console.log(notificationData);
  //   });
  //   this.oneSignal.handleNotificationOpened().subscribe((notificationData:any) => {
  //     // do something when a notification is opened
  //     console.log(notificationData);
  //   });
  //   this.oneSignal.endInit();
    
  //   this.oneSignal.getIds().then(identity => {
  //     console.log(identity);
  //     this.deviceToken = identity.userId;
  //   });

  //   // console.log(this.device);
  //   // this.cs.deviceid = this.device.uuid;
  // }
}
