import { Component, NgZone, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { NavigationExtras } from '@angular/router';
import { IAPProduct, InAppPurchase2 } from '@awesome-cordova-plugins/in-app-purchase-2/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.page.html',
  styleUrls: ['./subscriptions.page.scss'],
})
export class SubscriptionsPage implements OnInit {
  selectedIndex: any;
  planList: any = [];
  planid: any = '';
  planprice:any;
  planname:any="";
  ios_plan_name:any="";

  constructor(
    public navController: NavController, 
    public global: GlobalService, 
    private apiService: ApiService,
    public store:InAppPurchase2,
    public zone:NgZone,
    private nativeStorage: NativeStorage
  ) {
  }

  ngOnInit() {
    this.getPlans();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }
  
  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  /* ========= Inapp purchases configuration function ========= */
  public configureStart(){
    this.global.platform.ready()
    .then(()=>{
      // console.log(" .... Platform is Ready now ......");
        // this.store.verbosity = this.store.DEBUG;
        this.store.register({
          // id: "ponctuel_10_1m",
          id:this.ios_plan_name,
          type: this.store.NON_RENEWING_SUBSCRIPTION,
        });
        // this.store.when("ponctuel_10_1m")
        this.store.when(this.ios_plan_name)
          .approved(p => {
            this.global.dynamictimeToastHide();
            console.log("p.approved : ",JSON.stringify(p));
            p.verify()
          })
          .verified(p => {
            console.log("p.verified : ",JSON.stringify(p));
            p.finish()
          })
          .finished(p =>{
            // alert("Order finished : "+ JSON.stringify(p));
            if(p.transaction){
              if(p.transaction.id){
                // alert("Order finished : "+ JSON.stringify(p.transaction.id));
                // this.navController.navigateRoot(["/user-type-category"]);
                this.startSubmittingResponseData_forAppleInappPurchases(p)
              }
            }
          })
          .cancelled(p=>{
            this.global.dynamictimeToastHide();
            //message == > Payment canceled by the user
            // alert("Paiement annulé par l'utilisateur")
            this.global.presentToast("Paiement annulé par l'utilisateur")
          });
        this.store.refresh();

    })
    .catch((error)=>{
      console.log(" .... Platform is not ready");
    })
  }

  /* ========= Inapp purchases date store to database ========= */
  startSubmittingResponseData_forAppleInappPurchases(inApppurchases_response){
    this.global.presentLoadingDefault();
    let usertoken = this.global.userdetails.user_token;
    this.apiService.submitInappPurchasesResponse(this.planid, inApppurchases_response)
    .then((data: any) => {
      console.log("completePayment API data : ",data);
      // this.global.presentToast(data.message);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.global.presentToast(data.message);
          if(data.data.payment_status) {
            this.zone.run(() => {
              this.global.isLoggedIn = true;
              this.global.userdetails = data.data;
              this.global.userdetails.user_token = usertoken;
              this.global.user_access_token = usertoken;
              this.nativeStorage.setItem('userdetails', this.global.userdetails)
              .then((data) => {
                console.log('Stored item!', data);
                this.navController.navigateRoot(["/user-type-category"]);
              },
              error => console.error('Error storing item', error)
              );
            });
          }
        }
      }else{
        if(data.message){
          this.global.presentToast(data.message);
        }
      }
    })
    .catch((error)=>{
      this.global.presentLoadingClose();
      console.log("completePayment Error : ", error);
    })

  }





  selectItem(index:any, plan:any){
    this.global.presentLoadingDefault();
    console.log(index);
    console.log(plan);
    this.selectedIndex = index;
    this.planid = plan.id;
    this.planprice = plan.price;
    this.planname = plan.name;
    this.ios_plan_name = plan.ios_plan_name;

    if(plan.ios_plan_name){
      this.ios_plan_name = plan.ios_plan_name;
      if(this.global.platform.is('ios') == true){
        this.configureStart();
        setTimeout(() => {
          this.zone.run(()=>{
            this.global.presentLoadingClose();
          })
        }, 3500);
      }else{
        setTimeout(() => {
          this.zone.run(()=>{
            this.global.presentLoadingClose();
          })
        }, 200);
      }
    }else{
      setTimeout(() => {
        this.zone.run(()=>{
          this.global.presentLoadingClose();
        })
      }, 200);
    }
  }

  buySubscription() {
    let navigationExtras: NavigationExtras = {
      state: {
        planid: this.planid,
        planprice:this.planprice,
        planname:this.planname,
        ios_plan_name:this.ios_plan_name

      }
    };
    this.navController.navigateForward(["/payment"], navigationExtras);
  }

  getPlans() {
    this.global.presentLoadingDefault();
    this.apiService.subscriptionPlans().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.planList = data.data;
          this.selectItem(0, data.data[0]);
        }
      }
    })
  }


  /* === Buy Inapp Purchases ===== */ 
  buyinAppPurchanses(){
    this.global.dynamictimeToastshow("Traitement en cours, veuillez patienter...")
    this.store.order(this.ios_plan_name)
    .then(success=>{
      console.log("order success  : ", JSON.stringify(success))
    })
    .catch(error=>{
      this.global.dynamictimeToastHide();
      console.log("Order error : ", JSON.stringify(error));
    })
  }


}
