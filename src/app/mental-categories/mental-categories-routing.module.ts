import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MentalCategoriesPage } from './mental-categories.page';

const routes: Routes = [
  {
    path: '',
    component: MentalCategoriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MentalCategoriesPageRoutingModule {}
