import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MentalCategoriesPageRoutingModule } from './mental-categories-routing.module';

import { MentalCategoriesPage } from './mental-categories.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MentalCategoriesPageRoutingModule
  ],
  declarations: [MentalCategoriesPage]
})
export class MentalCategoriesPageModule {}
