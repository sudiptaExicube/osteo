import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-mental-categories',
  templateUrl: './mental-categories.page.html',
  styleUrls: ['./mental-categories.page.scss'],
})
export class MentalCategoriesPage implements OnInit {
  selectedIndex:any;
  mentalCategories: any = [];
  selectedCategory: any = {};


  constructor(public navController: NavController, public global: GlobalService, private apiService: ApiService) { }

  ngOnInit() {
    this.getMentalCategories();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  selectItem(index:any, category:any){
    console.log("index >", index);
    console.log("category >", category);
    this.selectedCategory = category;
    this.selectedIndex = index;
    this.goToPhysicalExercise();
  }

  goToPhysicalExercise() {
    let navigationExtras: NavigationExtras = {
      state: {
        category: this.selectedCategory
      }
    };
    this.navController.navigateForward(["/mental-videos"], navigationExtras);
  }

  getMentalCategories() {
    this.global.presentLoadingDefault();
    this.apiService.mentalCategories().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.mentalCategories = data.data;
        // this.selectItem(0, data.data[0]);          
        }
      }
    })
  }
}
