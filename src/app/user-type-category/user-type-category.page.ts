import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-user-type-category',
  templateUrl: './user-type-category.page.html',
  styleUrls: ['./user-type-category.page.scss'],
})
export class UserTypeCategoryPage implements OnInit {

  constructor(
    public global: GlobalService, 
    public navController: NavController,
    public apiService:ApiService,
    private iab: InAppBrowser,
    private nativeStorage:NativeStorage
  ) { }

  ngOnInit() {
    this.global.rootpage = 'type';
  }

  ionViewDidEnter() {
    this.fetchAppVersions();
  }

  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  fetchAppVersions(){
    // console.log("this.global.userdetails.user_id : ", this.global.userdetails.id);
    this.apiService.getAppVersionsWithuserId(this.global.userdetails.id)
    .then((success:any)=>{
      console.log("SUccess : ", success);
      if(success.status == true){
        if(success.data){
            this.global.android_version = success.data[0].android_version;
            this.global.ios_version = success.data[0].ios_version
            if(success.subscription == false){
                this.callTokenExpFunc()
            }else{
              if(success.data[0].check_version == "true"){
                this.fecthVersionHistory();
              }

              // this.fecthVersionHistory();
            }
        }
      }else{
        this.global.presentToast(success.message);
      }
    })
    .catch((error:any)=>{
      console.error(error);
      this.global.presentToast("Unable to fetch app version. Please check your internet connection or Restart the App")
    })
  }

  fecthVersionHistory(){
    this.global.appVersion.getVersionNumber()
    .then((value:any)=>{
      console.log("Version value is : ", value);
      if(this.global.platform.is('ios') == true){
        if(value != this.global.ios_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.ios_app_url)
        }
        
      }else if(this.global.platform.is('android') == true){
        if(value != this.global.android_version){
          // let appupdateUrl = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza";
          this.showVersionUpdateAlert(this.global.android_app_url)
        }
      }
    })
    .catch((error:any)=>{
      console.log("Version error is : ", error);
    })
  }

  showVersionUpdateAlert(appupdateUrl){
    this.global.versionAlert("Une nouvelle version est disponible. Veuillez mettre à jour l'application maintenant")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        if(this.global.platform.is('android') == true){
          navigator['app'].exitApp();
        }
        // navigator['app'].exitApp();
        // window.open(appupdateUrl,"_system")
        const options: InAppBrowserOptions = { zoom: 'no', clearcache:'yes', }
        const browserOne = this.iab.create(appupdateUrl, '_system', options);
      }
    })
  }






  goToProblem() {
    this.navController.navigateForward(["/problem-category"]);
  }

  childExerciseList() {
    this.navController.navigateForward(["/child-videos"]);
  }
}
