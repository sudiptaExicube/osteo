import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserTypeCategoryPageRoutingModule } from './user-type-category-routing.module';

import { UserTypeCategoryPage } from './user-type-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserTypeCategoryPageRoutingModule
  ],
  declarations: [UserTypeCategoryPage]
})
export class UserTypeCategoryPageModule {}
