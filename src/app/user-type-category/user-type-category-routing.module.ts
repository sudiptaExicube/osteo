import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserTypeCategoryPage } from './user-type-category.page';

const routes: Routes = [
  {
    path: '',
    component: UserTypeCategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserTypeCategoryPageRoutingModule {}
