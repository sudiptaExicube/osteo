import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-problem-category',
  templateUrl: './problem-category.page.html',
  styleUrls: ['./problem-category.page.scss'],
})
export class ProblemCategoryPage implements OnInit {
  issuesCategories: any = [];

  constructor(public navController: NavController, public global: GlobalService, private apiService: ApiService) { }

  ngOnInit() {
    this.getIssuesCategories();
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }

  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  getIssuesCategories() {
    this.global.presentLoadingDefault();
    this.apiService.issuesList().then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.issuesCategories = data.data;
        }
      }
    })
  }

  goToPage(ctype:any) {
    if(ctype == 'valeria')
      this.navController.navigateForward(["/human-body"]);
    else if(ctype == 'mental')
      this.navController.navigateForward(["/mental-categories"]);
    else if(ctype == 'hygienic')
      this.navController.navigateForward(["/hygienic-videos"]);
  }

  // goToMental() {
  //   this.navController.navigateForward(["/mental-categories"]);
  // }

  // goToHygine() {
  //   this.navController.navigateForward(["/hygienic-videos"]);
  // }
}
