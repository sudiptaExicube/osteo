import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProblemCategoryPageRoutingModule } from './problem-category-routing.module';

import { ProblemCategoryPage } from './problem-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProblemCategoryPageRoutingModule
  ],
  declarations: [ProblemCategoryPage]
})
export class ProblemCategoryPageModule {}
