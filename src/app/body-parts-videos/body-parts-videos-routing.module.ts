import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BodyPartsVideosPage } from './body-parts-videos.page';

const routes: Routes = [
  {
    path: '',
    component: BodyPartsVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BodyPartsVideosPageRoutingModule {}
