import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BodyPartsVideosPageRoutingModule } from './body-parts-videos-routing.module';

import { BodyPartsVideosPage } from './body-parts-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BodyPartsVideosPageRoutingModule
  ],
  declarations: [BodyPartsVideosPage]
})
export class BodyPartsVideosPageModule {}
