import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {
  contactForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public navController: NavController, public global: GlobalService, private apiService: ApiService) {
		this.contactForm = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z\s]*$/)
      ])],
      message: ['', Validators.required],
      telephone: ['', [Validators.compose([Validators.pattern(/^([0|\+[0-9]{1,5})?([0-9][0-9]{8})$/), Validators.required])]],
      email: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  //For token expire alert
  public callTokenExpFunc(){
    this.global.tokenExpireAlert("Jeton expiré. Veuillez vous reconnecter")
    .then((success:any)=>{
      if(success == 1){
        // navigator['app'].exitApp();
      }else if(success == 2){
        this.global.isLoggedIn = false;
        this.global.userdetails = {};
        this.global.user_access_token = '';
        this.global.nativeStorage.setItem('userdetails', null);
        this.navController.navigateRoot('/login');
      }
    })
  }
  public forceLogout(){
    this.global.presentToast("Jeton expiré. Veuillez vous reconnecter");
    this.global.isLoggedIn = false;
    this.global.userdetails = {};
    this.global.user_access_token = '';
    this.global.nativeStorage.setItem('userdetails', null);
    this.navController.navigateRoot('/login');
  }

  send() {
    console.log(this.contactForm.value);
    this.global.presentLoadingDefault();
    this.apiService.contactForm(this.contactForm.value).then((data:any) => {
      console.log(data);
      this.global.presentLoadingClose();
      this.global.presentToast(data.message);
      if(data.status) {
        if(data.status ==400){
          if(data.error){
            this.global.presentToast(data.error.message);
          }
        }else if(data.status ==401){
          // this.callTokenExpFunc();
          this.forceLogout();
        }else{
          this.navController.back();
        }
      }
    });
  }
}
