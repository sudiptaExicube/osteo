import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChildVideosPageRoutingModule } from './child-videos-routing.module';

import { ChildVideosPage } from './child-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChildVideosPageRoutingModule
  ],
  declarations: [ChildVideosPage]
})
export class ChildVideosPageModule {}
