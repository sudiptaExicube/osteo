import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChildVideosPage } from './child-videos.page';

const routes: Routes = [
  {
    path: '',
    component: ChildVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChildVideosPageRoutingModule {}
